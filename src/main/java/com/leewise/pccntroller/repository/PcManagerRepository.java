package com.leewise.pccntroller.repository;


import com.leewise.pccntroller.entity.PcManager;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PcManagerRepository extends JpaRepository<PcManager,Long> {
}
