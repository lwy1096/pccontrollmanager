package com.leewise.pccntroller.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class PcManager {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(nullable = false , length = 30)
    private String name;

    @Column(nullable = false, length = 20)
    private String pcName;

    @Column(nullable = false , length = 30)
    private String manageNum;
}
