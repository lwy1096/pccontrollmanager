package com.leewise.pccntroller.service;

import com.leewise.pccntroller.entity.PcManager;
import com.leewise.pccntroller.repository.PcManagerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PcManagerService {
    private final PcManagerRepository pcManagerRepository;

    public void setPcManagerService(String name , String pcName , String manageNum) {
        PcManager addData = new PcManager();
        addData.setName(name);
        addData.setPcName(pcName);
        addData.setManageNum(manageNum);

        pcManagerRepository.save(addData);
    }
}
