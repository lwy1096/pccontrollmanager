package com.leewise.pccntroller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PcCntrollerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PcCntrollerApplication.class, args);
	}

}
