package com.leewise.pccntroller.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PcManagerRequest {
    private String name;
    private String pcName;
    private String manageNum;
}
