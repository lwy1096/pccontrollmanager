package com.leewise.pccntroller.controller;

import com.leewise.pccntroller.model.PcManagerRequest;
import com.leewise.pccntroller.service.PcManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pcmanager")
public class PcManagerController {
    private final PcManagerService pcManagerService;

    @PostMapping("/setmanage")
    public String setPcManager(@RequestBody PcManagerRequest request) {
        pcManagerService.setPcManagerService( request.getName() , request.getPcName(), request.getManageNum());

        return "The Programs are already started";
    }


}
